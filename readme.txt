=== Jadwiga Problem Overlay ===
Contributors: wojtokuba
Tags: jadwiga-lublin
Requires at least: 5.0
Tested up to: 5.1.1
Stable tag: 5.0
Requires PHP: 7.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Jadwiga popup plugin used to display notifications about planned maintenance work.

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/jadwiga-problem-overlay` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress


== Changelog ==

= 1.0 =
* Published plugin

= 1.1 =
* Added dynamic loading from 3rd party website
* Added version checking
* Added source IP limits

= 1.1.1 =
* Testing automatic updates

= 1.2 =
* Moved source IP limits to API in order to better caching

= 1.3 =
* Fixed style loading error

= 1.3.1 =
* Updated plugin name
