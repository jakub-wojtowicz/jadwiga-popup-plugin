<?php
if ( !function_exists( 'add_action' ) ) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}

class JDProblemPlugin {
    private static $JDProblemInstance;
    private static $IPRange = ["87.246.232.0/29", "109.200.89.36/29"];

    public function __construct()
    {
        $this->init();
    }

    public static function getInstance(){
        if(!self::$JDProblemInstance){
            self::$JDProblemInstance = new JDProblemPlugin();
        }

        return self::$JDProblemInstance;
    }

    private function init(){
        $this->constants();
        $this->registerHooks();
        $this->initializeAssets();

    }

    private function registerHooks(){
        add_action('wp_footer', [&$this, 'attachOverlayBox']);
        add_action('wp_enqueue_scripts', [&$this, 'initializeAssets'] );
    }

    private function constants(){
        define('JDPP_VERSION', '1.3.1');
        define('JDPP_PREFIX', 'JDPP');
        define('JDPP_MINIMUM_WP_VERSION', '5.0' );
    }

    public final function attachOverlayBox($name){
        ?>
        <div class="modal micromodal-slide" id="modal-maintenance" aria-hidden="true">
            <div class="modal__overlay" tabindex="-1" data-micromodal-close>
                <div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="modal-1-title">
                    <header class="modal__header">
                        <h2 class="modal__title" id="modal-1-title">
                            Uwaga! Trwają prace techniczne!
                        </h2>
                    </header>
                    <main class="modal__content" id="modal-1-content">
                        <p>
                            Mogą wystąpić problemy w dostępie do usług. Prace w trakcie poniżej:
                        </p>
                        <p class="problems">
                        </p>
                        <span style="font-size:17px;">(tą informację widzą tylko osoby na terenie Jadwigi)</span><br>
                        <span style="font-size:13px;">*[LBN-DLG] - lok. Długosza, [LBN-WRB] - lok. Wróbla, [Backend/Core] - rdzeń sieci, [Whole Network] - cała infrastruktura</span>
                    </main>
                    <footer class="modal__footer">
                        <button class="modal__btn modal__btn-primary" data-micromodal-acknowledge>Rozumiem</button>
                    </footer>
                </div>
            </div>
        </div>
        <?php
    }

    public final function initializeAssets(){
        wp_enqueue_style('JDPP_css_main',plugins_url('css/style.css',__FILE__ ), null, JDPP_VERSION);
        wp_enqueue_script('JDPP_js_micromodal',  plugins_url('js/micromodal.min.js',__FILE__ ));
        wp_enqueue_script('JDPP_js_problem', plugins_url('js/problem.js',__FILE__ ), array( 'jquery' ), JDPP_VERSION, true);
    }

}

