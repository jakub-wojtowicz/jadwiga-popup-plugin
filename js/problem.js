jQuery(document).ready(function( $ ) {
    if(getCookie('JDPP_already_displayed') === ''){
        $.ajax({
           method: "GET",
           url: "https://lan.edja.pl/app/getRecentProblems",
           success: function(res) {
               if(Object.keys(res).length > 0){
                   var problems = $('#modal-maintenance .problems');
                   problems.empty();
                   for(let line of res){
                       problems.append(`<code><a href="https://online.hsnet.pl/index.php?do=details&task_id=${line.id}" target="_blank">[${line.concern}]* - ${line.name}</a></code><br>`);
                   }
                   MicroModal.show('modal-maintenance', {
                       onClose: function (modal) {
                           setCookie('JDPP_already_displayed', 1, 1)
                       }
                   });
               }
           }
        });
    }

    $('button[data-micromodal-acknowledge]').click(function(){
        MicroModal.close('modal-maintenance');
    });


});

function setCookie(cookieName, cookieValue, cookieExdays) {
    var d = new Date();
    d.setTime(d.getTime() + (cookieExdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cookieName + "=" + cookieValue + ";" + expires + ";path=/";
}

function getCookie(cookieName) {
    var name = cookieName + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length,c.length);
        }
    }
    return "";
}