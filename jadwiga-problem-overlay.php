<?php
/**
 * Plugin Name: Trouble List
 * Plugin URI: https://gitlab.com/2cw/jadwiga-lublin/jadwiga-popup-plugin
 * Description: Popup plugin used to display notifications about planned maintenance work.
 * Version: 1.3.1
 * Author: HSNet.pl
 * Author URI: https://www.hsnet.pl
 */

if ( !function_exists( 'add_action' ) ) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}
define( 'JDPP__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

require_once JDPP__PLUGIN_DIR . 'Problem.class.php';
require JDPP__PLUGIN_DIR.'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://gitlab.com/2cw/jadwiga-lublin/jadwiga-popup-plugin/',
    __FILE__,
    'jdpp-wp-plugin'
);

//Optional: Set the branch that contains the stable release.
$myUpdateChecker->setBranch('master');

$JDProblemPlugin = JDProblemPlugin::getInstance();
